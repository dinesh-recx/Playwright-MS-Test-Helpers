# Playwright

1. Create a repository and an NUnit Project
2. Add Microsoft.Playwright.NUnit package to your project
3. Build the project
4. On your repo folder/project and run this PowerShell command pwsh bin/Debug/netX/playwright.ps1 install
5. If pwsh is not available, you have to install PowerShell.


https://playwright.dev/dotnet/docs/intro
